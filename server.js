const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

const app = express();

const userRoute = require('./routes/user');
const authRoute = require('./routes/auth');
const profileRoute = require('./routes/profile');
const postRoute = require('./routes/post');

require('dotenv/config');

mongoose.connect(process.env.connection, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});
const db = mongoose.connection;
db.on('error', (err) => {
  console.log(err);
});
db.once('open', () => {
  console.log('Database connection established');
});

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({
  extended: true,
}));
app.use(express.json());
app.use(cors());

app.use(userRoute);
app.use(authRoute);
app.use(profileRoute);
app.use(postRoute);

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
