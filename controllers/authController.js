const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/User');

exports.post = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });

  try {
    if (!user) {
      res.status(400).json({ errors: [{ msg: 'Invalid credentials ' }] });
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      res.status(400).json({ errors: [{ msg: 'Password invalid' }] });
    }
    const payload = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(payload, process.env.jwtSecret,
      { expiresIn: 360000 },
      (err, token) => {
        if (err) throw err;
        res.json({ token });
      });
  } catch (err) {
    res.json({ message: err });
  }
};

exports.get = async (req, res) => {
  try{
      const user = await User.findById(req.user.id).select('-password');
      res.json(user);
  }
  catch(err){
      res.status(500).send('Server Error');
  }
};