const express = require('express');
const Profile = require('../models/Profile');
const User = require('../models/User');



exports.post=async(req,res) => {

    const user = await User.findById(req.user.id).select('-password');

    try {
        const newProfile = new Profile({
        image: req.file.path,
        location:req.body.location,
        bio:req.body.bio,
        user: req.user.id,
        });

        const profile = await newProfile.save();

        res.json(profile);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
};

exports.getAll = async (req,res) => {
    try{
        const profiles = await Profile.find().populate('user',['name']);
        res.json(profiles);
    } catch(err){
        res.status(500).send('Server Error');
    }
};

exports.getId = async (req,res) => {
    try{
        const profile = await Profile.findOne({ user: req.params.user_id }).populate('user',['name']);
        res.json(profile);

        if(!profile) return res.status(400).json({msg: 'There is no profile for this user'});
    } catch(err){
        console.error(err.message);
        if(err.kind == 'ObjectId'){
            return res.status(400).json({msg: 'Profile not Found'});
        }
        res.status(500).send('Server Error');
    }
};

exports.delete =  async (req,res) => {
    try{
        await Profile.findOneAndRemove({ user: req.user.id});
        await User.findOneAndRemove({ _id: req.user.id});
        res.json({msg: 'User deleted'});
    } catch(err){
        res.status(500).send('Server Error');
    }
};


