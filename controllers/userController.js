const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/User');

exports.post = async (req, res) => {
  const { name, email, password } = req.body;
  let user = await User.findOne({ email });

  try {
    if (user) {
      return res.status(400).json({ errors: [{ msg: 'User already exists' }] });
    }
    user = new User({
      name,
      email,
      password,
    });
    const saltPassword = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, saltPassword);
    await user.save();

    const payload = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(payload, process.env.jwtSecret,
      { expiresIn: 360000 },
      (err, token) => {
        if (err) throw err;
        res.json({ token });
      });
  } catch (err) {
    res.json({ message: err });
  }
};

exports.get = async (req, res) => {
  try {
    const Alluser = await User.find();
    res.json(Alluser);
  } catch (err) {
    res.json({ message: err });
  }
};

exports.request = async (req, res) => {
  try {
    const { requestId } = req.body;
    const userId = req.body.UserId;

    await User.updateOne(
      { _id: requestId },
      { $push: { request: requestId } },
      { safe: true, upset: true },
    );
    await User.updateOne(
      { _id: userId },
      { $push: { givenrequest: userId } },
      { safe: true },
      { upset: true },
    );
    res.status(200).json({
      message: 'Update request to the user',
    });
    res.json(User.followers);
  } catch (err) {
    res.status(404).json({
      message: 'Server Error',
    });
  }
};

exports.follower = async (req, res) => {
  try {
    const { requestId } = req.body;
    const { userId } = req.body;

    await User.updateOne(
      { _id: userId },
      { $push: { following: requestId } },
      { safe: true, upset: true },
    );
    await User.updateOne(
      { _id: requestId },
      { $push: { follower: userId } },
      { safe: true, upset: true },
    );
    res.status(200).json({
      message: 'Add followers and following to the user',
    });
    res.json(User.followers);
  } catch (err) {
    res.status(404).json({
      message: 'Server Error',
    });
  }
};
exports.frnd = async (req, res) => {
  try {
    const friend = await User.follower.findById(req.params.id);
    if (!friend) {
      return res.status(404).json({ msg: 'Friend not found' });
    }
    res.json(friend);
  } catch (err) {
    if (err.kind === 'ObjectId') {
      return res.status(404).json({ msg: 'Friend not found' });
    }
    res.status(500).send('Server error');
  }
};
