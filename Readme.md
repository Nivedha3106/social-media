SOCIAL MEADIA APPLICATION

Build RESTful APIs for a social media application.

OBJECTIVE:
Buliding APIs for social media app where the person can create profile, add post and also CRUD operations.

FEATURES:
    1.Login and SignUp fro the user.<br>
    2.Once the user logged in, create new profile and fill the necessary credentials.<br>
    3.User can feed the post and can like, unlike post of others.<br>
    4.Request can be given by the user to their friend.<br>
    5.Once Request accepted,the following and followers is updated.<br>
    6.Search friends<br>

TECH STACK USED:
    MongoDB, Express, Node, Heroku

Deployed URL: 
    https://social-media-appl.herokuapp.com/
