exports.postValidate =(req,res,next) => {
  const {
      text
  } = req.body;
  if(!text) {
    return res.status(400).json ({error: 'please fill all the fields!!'});
  }
  next();
};

exports.commentValidate = (req,res,next) => {
  const {
      text
  }=req.body;
  if(!text) {
    return res.status(400).json ({error: 'You have missed to comment!!'});
  }
  next();
};