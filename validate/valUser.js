exports.signupValidate =(req,res,next) => {
  const {
      name,email,password
  } = req.body;
  if(!email || !password || !name) {
    return res.status(400).json ({error: 'please fill all the fields!!'});
  }
  const len=password.length >= 6;
  if(!len) {
    return res.status(400).json ({error: 'password should contain atleast 6 characters'});
  }
  if(len) {
    next();
  }
};



