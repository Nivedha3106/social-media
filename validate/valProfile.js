exports.profileValidate =(req,res,next) => {
    const {
        location,bio 
    } = req.body;
    const image = req.file.path;
    if(!location || !bio || !image) {
      return res.status(400).json ({error: 'please fill all the fields!!'});
    }
    next();
};
