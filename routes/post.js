const express = require('express');
const router = express.Router();
const multer = require('multer');
const auth = require('../middleware/auth');
const Post = require('../models/Post');
const postController = require('../controllers/postController')
const {postValidate, commentValidate} = require('../validate/valPost');

const storage = multer.diskStorage({
  destination(cb) {
    cb(null, './uploads');
  },
  filename(req, file, cb) {
    cb(null, file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  const allowedFileTypes = ['image/jpeg', 'image/jpg', 'image/png'];
  if (allowedFileTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const Upload = multer({ storage, fileFilter });

router.post('/post', Upload.single('image'), auth,postValidate, postController.post);

router.get('/get', auth,postController.get);
router.get('/getPost/:id', auth, postController.getId);

router.delete('/delete/:id', auth, postController.delete);

router.put('/like/:id', auth, postController.like);

router.put('/unlike/:id', auth, postController.unlike);

router.post('/comment/:id', auth, commentValidate,postController.comment);

router.delete('/delcomment/:id/:comment_id', auth, postController.delcomment);
router.delete('/delAll',auth,postController.delAll);
  
module.exports = router;
