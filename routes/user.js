const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const userController = require('../controllers/userController');
const {signupValidate} = require('../validate/valUser');

router.post('/user',signupValidate,userController.post);

router.get('/user', userController.get);

router.post('/request',auth,userController.request);

router.post('/follower',auth, userController.follower);

router.get('/frnd/:id', auth,userController.frnd);

module.exports = router;
