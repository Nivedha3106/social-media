const express = require('express');
const router = express.Router();
const multer = require('multer');
const auth = require('../middleware/auth');
const profileController = require('../controllers/profileController');
const {profileValidate} = require('../validate/valProfile');


const storage = multer.diskStorage({
    destination(cb) {
      cb(null, '../uploads');
    },
    filename(req, file, cb) {
      cb(null, file.originalname);
    },
});
  
const fileFilter = (req, file, cb) => {
const allowedFileTypes = ['image/jpeg', 'image/jpg', 'image/png','image/jfif'];
if (allowedFileTypes.includes(file.mimetype)) {
    cb(null, true);
} else {
    cb(null, false);
}
};
  
const Upload = multer({ storage, fileFilter });

router.post('/profile', Upload.single('image'),auth, profileValidate,profileController.post);

router.get('/profile', profileController.getAll);

router.get('/profile/:id', profileController.getId);

router.delete('/profile', auth, profileController.delete);

module.exports = router;
