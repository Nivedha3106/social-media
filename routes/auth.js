const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const authController = require('../controllers/authController');
const {authValidate} = require('../validate/valAuth');

router.post('/auth', authValidate,authController.post);
router.get('/auth',auth,authController.get);

module.exports = router;