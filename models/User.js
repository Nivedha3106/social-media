const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
      type: String,
    },
    email: {
      type: String,
    },
    password: {
      type: String,
    },
    date: {
      type: Date,
      default: Date.now,
    },
    request : [{ 
      type : mongoose.Schema.Types.ObjectId,
      ref : 'users',
    }],
    givenrequest : [{
      type : mongoose.Schema.Types.ObjectId,
      ref : 'users',
    }],
    follower: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    }],
    following: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    }],
    
  });


module.exports = mongoose.model("user", userSchema);