const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    image :{
        type:String,
    },
    location: {
    type: String
    },
    bio: {
    type: String
    },

});
    
module.exports = mongoose.model('profile', ProfileSchema);
