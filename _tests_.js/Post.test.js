const assert = require('assert');

const {fakeData} = require('./fakedata');
const User = require('../models/User');
const Post = require('../models/Post');
const { connect,disconnect } = require('./dbHandler');

let result;
let done;
let postId;

describe('Test for Post', () => {
    beforeEach(() => {
        jest.setTimeout(10 * 10000);
    });

    beforeAll(async () => {
        connect();
    });

    afterAll (async () => {
        disconnect();
        await new Promise(
            (resolve) => setTimeout(() => resolve(), 10000));
    });

    it('creation of post', async () => {
        try {
            const user = await Post.findOne({email: fakeData.email});
            const post = new Post ({
                text: 'test',
                image: 'url',
                postBy: user._id
            });
            await post.save();
            if(post) {
                assert.ok(result.statusCode === 200);
                done();
            }
        }catch (err) {
            expect(201);
        }
    });

    it('find the user who posted the post', async () => {
        try{
            const post = await Post.findOne(postId);
            if(post) {
                assert.ok(result.statusCode === 200);
                done();
            }
        } catch (err) {
            expect(201);
        }
    });

    it('Likes',async() => {
        try{
            const likes = await Post.findOne(postId,{$push: 
                { likes: testId }
            });
            if(likes) {
                assert.ok(result.statusCode === 200);
                done();
            }
        } catch (err) {
            expect(201);
        }
    });

    it('Unlike', async () => {
        try {
            const unlike = await Post.findOne(postId,{
                $pull:{unlike:testId}
            });
            if(unlike) {
                assert.ok(result.statusCode === 200);
                done();
            }
        }catch (err) {
            expect(201);
        }
    });

    it('comment the post',async() =>{
        try {
            const user = await Post.findOne(postId);
            const comment = new Comment ({
                text:'test'
            });
            await comment.save();
            if(comment) {
                assert.ok(result.statusCode === 200);
                done();
            }
        }catch (err) {
            expect(201);
        }
    });

    it('should delete the post' , async() => {
        try{
            const post = await Post.deleteOne({email : fakeData.email});
            if(post) {
                assert.ok(result.statusCode === 200);
                done();
            }
        } catch(err) {
            expect(201);
        }
    });
});