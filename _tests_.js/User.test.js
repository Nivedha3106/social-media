const assert = require('assert');

const {fakeData} = require('./fakedata');
const User = require('../models/User');
const { connect,disconnect } = require('./dbHandler');
require('dotenv/config');

let result;
let done;
let testId;

describe('Test for User', () => {
    beforeEach(() => {
        jest.setTimeout(10 * 10000);
    });

    beforeAll(async () => {
        connect();
    });

    afterAll (async () => {
        disconnect();
        await new Promise(
            (resolve) => setTimeout(() => resolve(), 10000));
    });

    it('should get all the user details', async () => {
        try {
            const user = await User.findOne({ email: fakeData.email });
            testId= user._id;
            if (user) {
                assert.ok(result.statusCode === 200);
                done();
            }
        } catch (err) {
            expect(201);
        }
            
    });

    it('it should get request to the user', async() => {
        try{
            const request = await User.findOne({request: testId});
            if(request) {
                assert.ok(result.statusCode === 200);
                done();
            }
        } catch(err) {
            expect(201);
        }
    });

    it('send request to the user',async () => {
        try {
            const request = await User.findOne(testId,{$push: 
                { request: testId }
            });
            if (request){
                assert.ok(result.statusCode === 200);
                done();
            }
        } catch(err) {
            expect(201);
        }
    });

    it('should add a follower in user',async () => {
        try{
            const follower = User.updateOne({ _id: process.env.userId },
                { $push: { followers: testId } }, { new: true, safe: true })
            if(follower) {
                assert.ok(result.statusCode === 200);
                done();
            }   
        }catch(err) {
            expect(201);
        }
    });

    it('should search a friend who is follwing the user', async () => {
        try {
            const friend = User.findOne({follower: testId});
            if(friend) {
                assert.ok(result.statusCode === 200);
                done();
            }
        } catch(err) {
            expect(201);
        }
    });
})