const assert = require('assert');

const {fakeData} = require('./fakedata');
const Profile = require('../models/Profile');
const { connect,disconnect } = require('./dbHandler');

let result;
let done;
let profileId;

describe('Test for Profile', () => {
    beforeEach(() => {
        jest.setTimeout(10 * 10000);
    });

    beforeAll(async () => {
        connect();
    });

    afterAll (async () => {
        disconnect();
        await new Promise(
            (resolve) => setTimeout(() => resolve(), 10000));
    });
    it('should create profile for user',async ()=>{
        try{
            const user = await Profile.findOne({ email: fakeData.email });
            const  profile = new Profile ({
                image:'url',
                location:'test',
                bio:'test',
                profileId:profile._id
            });
            await profile.save();
            if(profile) {
                assert.ok(result.statusCode === 200);
                done();
            }
        }catch (err) {
            expect(201);
        }
    });

    it('should update the profile', async () => {
        try {
            const updateData = {
                image,
                location,
                bio,
            };
            const updates = await Profile.findByIdAndUpdate(testId, {
                $set: updateData,
            }, { new: true })
            if(updates) {
                assert.ok(result.statusCode === 200);
                done();
            }
        }catch(err) {
            expect(201);
        }
    });

    it('should delete the profile' , async() => {
        try{
            const profile = await Profile.deleteOne({email : fakeData.email});
            if(profile) {
                assert.ok(result.statusCode === 200);
                done();
            }
        } catch(err) {
            expect(201);
        }
    });
});